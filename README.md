# Anserini Pre-Built Indexes

This repository holds pre-built indexes, either to be used with [Anserini](http://anserini.io/) (from Java) or [Pyserini](http://pyserini.io/) (from Python).
Pyserini provides a [simple way to download these indexes](https://github.com/castorini/pyserini/blob/master/docs/prebuilt-indexes.md); the configuration information for this feature is stored [here](https://github.com/castorini/pyserini/blob/master/pyserini/prebuilt_index_info.py).

You can also manually download the indexes, for example:

```bash
# MS MARCO passage index
$ wget https://git.uwaterloo.ca/jimmylin/anserini-indexes/raw/master/index-msmarco-passage-20201117-f87c94.tar.gz

# Robust04
$ wget https://git.uwaterloo.ca/jimmylin/anserini-indexes/raw/master/index-robust04-20191213.tar.gz
```

Here are all the pre-built indexes that are available:

| Index | Readme |Download | MD5 |
|:------|:-------|:--------|:----|
| `robust04` | [[Link](https://git.uwaterloo.ca/jimmylin/anserini-indexes/raw/master/index-robust04-20191213-readme.txt)] | [[GitLab](https://git.uwaterloo.ca/jimmylin/anserini-indexes/raw/master/index-robust04-20191213.tar.gz)] [[Vault](https://vault.cs.uwaterloo.ca/s/eqFacNeSGc4pLLH)] | `15f3d001489c97849a010b0a4734d018`
| `msmarco-passage` | [[Link](https://git.uwaterloo.ca/jimmylin/anserini-indexes/raw/master/index-msmarco-passage-20201117-f87c94-readme.txt)] | [[GitLab](https://git.uwaterloo.ca/jimmylin/anserini-indexes/raw/master/index-msmarco-passage-20201117-f87c94.tar.gz)] [[Vault](https://vault.cs.uwaterloo.ca/s/QQsZMFG8MpF4P8M)] | `1efad4f1ae6a77e235042eff4be1612d`
| `msmarco-passage-slim` | [[Link](https://git.uwaterloo.ca/jimmylin/anserini-indexes/raw/master/index-msmarco-passage-slim-20201202-ab6e28-reademe.txt)] | [[GitLab](https://git.uwaterloo.ca/jimmylin/anserini-indexes/raw/master/index-msmarco-passage-slim-20201202-ab6e28.tar.gz)] [[Vault](https://vault.cs.uwaterloo.ca/s/Kx6K9NJFmwnaAP8)] | `5e11da4cebd2e8dda2e73c589ffb0b4c`
| `msmarco-passage-expanded` | [[Link](https://git.uwaterloo.ca/jimmylin/anserini-indexes/raw/master/index-msmarco-passage-expanded-20201121-e127fb-readme.txt)] | [[GitLab](https://git.uwaterloo.ca/jimmylin/anserini-indexes/raw/master/index-msmarco-passage-expanded-20201121-e127fb.tar.gz)] [[Vault](https://vault.cs.uwaterloo.ca/s/pm7cisJtRxiAMHd)] | `e5762e9e065b6fe5000f9c18da778565`
| `msmarco-doc` | [[Link](https://git.uwaterloo.ca/jimmylin/anserini-indexes/raw/master/index-msmarco-doc-20201117-f87c94-readme.txt)] | [[GitLab](https://git.uwaterloo.ca/jimmylin/anserini-indexes/raw/master/index-msmarco-doc-20201117-f87c94.tar.gz)] [[Vault](https://vault.cs.uwaterloo.ca/s/5NC7A2wAL7opJKH)] | `ac747860e7a37aed37cc30ed3990f273`
| `msmarco-doc-slim` | [[Link](https://git.uwaterloo.ca/jimmylin/anserini-indexes/raw/master/index-msmarco-doc-slim-20201202-ab6e28-readme.txt)] | [[GitLab](https://git.uwaterloo.ca/jimmylin/anserini-indexes/raw/master/index-msmarco-doc-slim-20201202-ab6e28.tar.gz)] [[Vault](https://vault.cs.uwaterloo.ca/s/BMZ6oYBoEPgTFqs)] | `c56e752f7992bf6149761097641d515a`
| `msmarco-doc-per-passage` | [[Link](https://git.uwaterloo.ca/jimmylin/anserini-indexes/raw/master/index-msmarco-doc-per-passage-20201204-f50dcc-readme.txt)] | [[GitLab](https://git.uwaterloo.ca/jimmylin/anserini-indexes/raw/master/index-msmarco-doc-per-passage-20201204-f50dcc.tar.gz)] [[Vault](https://vault.cs.uwaterloo.ca/s/q6sAxE6q57q2TBo)] | `797367406a7542b649cefa6b41cf4c33`
| `msmarco-doc-per-passage-slim` | [[Link](https://git.uwaterloo.ca/jimmylin/anserini-indexes/raw/master/index-msmarco-doc-per-passage-slim-20201204-f50dcc-readme.txt)] | [[GitLab](https://git.uwaterloo.ca/jimmylin/anserini-indexes/raw/master/index-msmarco-doc-per-passage-slim-20201204-f50dcc.tar.gz)] [[Vault](https://vault.cs.uwaterloo.ca/s/mKTjbTKMwWF9kY3)] | `77c2409943a8c9faffabf57cb6adca69`
| `msmarco-doc-expanded-per-doc` | [[Link](https://git.uwaterloo.ca/jimmylin/anserini-indexes/raw/master/index-msmarco-doc-expanded-per-doc-20201126-1b4d0a-readme.txt)] | [[GitLab](https://git.uwaterloo.ca/jimmylin/anserini-indexes/raw/master/index-msmarco-doc-expanded-per-doc-20201126-1b4d0a.tar.gz)] [[Vault](https://vault.cs.uwaterloo.ca/s/3BQz6ZAXAxtfne8)] | `f7056191842ab77a01829cff68004782`
| `msmarco-doc-expanded-per-passage` | [[Link](https://git.uwaterloo.ca/jimmylin/anserini-indexes/raw/master/index-msmarco-doc-expanded-per-passage-20201126-1b4d0a-readme.txt)] | [[GitLab](https://git.uwaterloo.ca/jimmylin/anserini-indexes/raw/master/index-msmarco-doc-expanded-per-passage-20201126-1b4d0a.tar.gz)] [[Vault](https://vault.cs.uwaterloo.ca/s/eZLbPWcnB7LzKnQ)] | `54ea30c64515edf3c3741291b785be53`
`enwiki-paragraphs` | | [[GitLab](https://git.uwaterloo.ca/jimmylin/anserini-indexes/raw/master/lucene-index.enwiki-20180701-paragraphs.tar.gz)] [[Vault](https://vault.cs.uwaterloo.ca/s/WHKMSCbwQfDXyHt)] | `77d1cd530579905dad2ee3c2bda1b73d`
`zhwiki-paragraphs` | | [[GitLab](https://git.uwaterloo.ca/jimmylin/anserini-indexes/raw/master/lucene-index.zhwiki-20181201-paragraphs.tar.gz)] [[Vault](https://vault.cs.uwaterloo.ca/s/6kEjQZaRYtnb8A6)] | `c005af4036296972831288c894918a92`
`cast2019` | | [[GitLab](https://git.uwaterloo.ca/jimmylin/anserini-indexes/raw/master/index-cast2019.tar.gz)] [[Vault](https://vault.cs.uwaterloo.ca/s/56LcDcRPopdQc4d)] | `36e604d7f5a4e08ade54e446be2f6345`
`wikipedia-dpr` | [[Link](https://git.uwaterloo.ca/jimmylin/anserini-indexes/raw/master/index-wikipedia-dpr-20210120-d1b9e6-readme.txt)] | [[GitLab](https://git.uwaterloo.ca/jimmylin/anserini-indexes/raw/master/index-wikipedia-dpr-20210120-d1b9e6.tar.gz)] [[Vault](https://vault.cs.uwaterloo.ca/s/t6tDJmpoxPw9tH8)] | `c28f3a56b2dfcef25bf3bf755c264d04`
`wikipedia-dpr-slim` | [[Link](https://git.uwaterloo.ca/jimmylin/anserini-indexes/raw/master/index-wikipedia-dpr-slim-20210120-d1b9e6-readme.txt)] | [[GitLab](https://git.uwaterloo.ca/jimmylin/anserini-indexes/raw/master/index-wikipedia-dpr-slim-20210120-d1b9e6.tar.gz)] [[Vault](https://vault.cs.uwaterloo.ca/s/Gk2sfTyJCyaTrYH)] | `7d40604a824b5df37a1ae9d25ea38071` 
